
#include <I2Cdev.h>
#include <MPU6050.h>
#include <Adafruit_MPU6050.h>
#include <Adafruit_Sensor.h>
#include <Wire.h>
#include <EEPROM.h>

//////////////////////////////////  DEFINIATION ///////////////////////////
#define EEPROM_SIZE 14
#define EEPROM_SENSOR_STATE 3
#define SW1 15
#define RELAY 27
#define BUZZER 4

Adafruit_MPU6050 mpu;
bool EEPROMFlag;  //if flag is false then offset value will be written in eeprom otherwise device will use previous offsets.

//////////////////////////////////   CONFIGURATION   /////////////////////////////
//Change this 3 variables if you want to fine tune the skecth to your needs.
int buffersize = 1000;   //Amount of readings used to average, make it higher to get more precision but sketch will be slower  (default:1000)
int acel_deadzone = 8;   //Acelerometer error allowed, make it lower to get more precision, but sketch may not converge  (default:8)
int giro_deadzone = 1;   //Giro error allowed, make it lower to get more precision, but sketch may not converge  (default:1)

// default I2C address is 0x68
// specific I2C addresses may be passed as a parameter here
// AD0 low = 0x68 (default for InvenSense evaluation board)
// AD0 high = 0x69
//MPU6050 accelgyro;
MPU6050 accelgyro(0x68); // <-- use for AD0 high

int16_t ax, ay, az, gx, gy, gz;

int16_t mean_ax, mean_ay, mean_az, mean_gx, mean_gy, mean_gz, state =  EEPROM.read(12);
int16_t ax_offset, ay_offset, az_offset, gx_offset, gy_offset, gz_offset;


///////////////////////////////////   SETUP   ////////////////////////////////////
void setup() {

  //Init EEPROM
  EEPROM.begin(EEPROM_SIZE); // initialize eeprom
  pinMode(SW1, INPUT);
  pinMode(RELAY, OUTPUT);
  pinMode(BUZZER, OUTPUT);

  // initialize serial communication
  Serial.begin(115200);
  Serial.println("start");
  // Try to initialize!
  if (!mpu.begin()) {
    Serial.println("Failed to find MPU6050 chip");
    while (1) {
      delay(10);
    }
  }
  Serial.println("MPU6050 Found!");
  // join I2C bus (I2Cdev library doesn't do this automatically)
  Wire.begin();

  // initialize device
  accelgyro.initialize();

  // wait for ready
  while (Serial.available() && Serial.read()); // empty buffer
  //  while (!Serial.available()){
  //    Serial.println(F("Send any character to start sketch.\n"));
  //    delay(1500);
  //  }
  while (Serial.available() && Serial.read()); // empty buffer again

  // start message
  Serial.println("\nMPU6050 Calibration Sketch");
  delay(2000);
  Serial.println("\nYour MPU6050 should be placed in horizontal position, with package letters facing up. \nDon't touch it until you see a finish message.\n");
  delay(3000);
  // verify connection
  Serial.println(accelgyro.testConnection() ? "MPU6050 connection successful" : "MPU6050 connection failed");
  delay(1000);
  // reset offsets
  accelgyro.setXAccelOffset(0);
  accelgyro.setYAccelOffset(0);
  accelgyro.setZAccelOffset(0);
  accelgyro.setXGyroOffset(0);
  accelgyro.setYGyroOffset(0);
  accelgyro.setZGyroOffset(0);


  // set accelerometer range to +-8G
  mpu.setAccelerometerRange(MPU6050_RANGE_8_G);

  // set gyro range to +- 500 deg/s
  mpu.setGyroRange(MPU6050_RANGE_500_DEG);

  // set filter bandwidth to 21 Hz
  mpu.setFilterBandwidth(MPU6050_BAND_21_HZ);
  delay(100);

  readOffsetFromEEPROM();// Reading previous offset values from eeprom
  state = EEPROM.read(12);
  if (state == 3)
  {
    EEPROMFlag = true;
  }


}

///////////////////////////////////   LOOP   ////////////////////////////////////
void loop() {







  if (state == 0) {
    Serial.println("\nReading sensors for first time...");
    meansensors();
    state++;
    delay(100);
  }

  if (state == 1) {
    Serial.println("\nCalculating offsets...");
    calibration();
    digitalWrite(BUZZER, HIGH); 
    delay(1000);        // ...for 1 sec
     digitalWrite(BUZZER, LOW);     // Stop sound...
    delay(1000);
    //readOffsetFromEEPROM();
    state++;
    delay(100);
  }

  if (state == 2) {
    meansensors();
    Serial.println("\nFINISHED!");
    Serial.print("\nSensor readings with offsets:\t");
    Serial.print(mean_ax);
    Serial.print("\t");
    Serial.print(mean_ay);
    Serial.print("\t");
    Serial.print(mean_az);
    Serial.print("\t");
    Serial.print(mean_gx);
    Serial.print("\t");
    Serial.print(mean_gy);
    Serial.print("\t");
    Serial.println(mean_gz);
    Serial.print("Your offsets:\t");
    Serial.print(ax_offset);
    Serial.print("\t");
    Serial.print(ay_offset);
    Serial.print("\t");
    Serial.print(az_offset);
    Serial.print("\t");
    Serial.print(gx_offset);
    Serial.print("\t");
    Serial.print(gy_offset);
    Serial.print("\t");
    Serial.println(gz_offset);
    if (EEPROMFlag == false)
    {
      writeOffsetToEEPROM();
      EEPROMFlag = true;
    }
    EEPROM.write(12, EEPROM_SENSOR_STATE);//to avoid calibration continuesly, writing value 3 in eeprom which further set state is equal to 3
    //readOffsetFromEEPROM();
    Serial.println("\nData is printed as: acelX acelY acelZ giroX giroY giroZ");
    Serial.println("Check that your sensor readings are close to 0 0 16384 0 0 0");
    Serial.println("If calibration was succesful write down your offsets so you can set them in your projects using something similar to mpu.setXAccelOffset(youroffset)");
    //while (1);
  }
  if (state == 3)
  {
    Serial.println("\nState 3 present reading from eeprom");
    readOffsetFromEEPROM();

  }
  /* Get new sensor events with the readings */
  sensors_event_t a, g, temp;
  mpu.getEvent(&a, &g, &temp);


  /* Print out the values */
  Serial.print("Acceleration X: ");
  Serial.print(a.acceleration.x);
  Serial.print(", Y: ");
  Serial.print(a.acceleration.y);
  Serial.print(", Z: ");
  Serial.print(a.acceleration.z);
  Serial.println(" m/s^2");

  Serial.print("Rotation X: ");
  Serial.print(g.gyro.x);
  Serial.print(", Y: ");
  Serial.print(g.gyro.y);
  Serial.print(", Z: ");
  Serial.print(g.gyro.z);
  Serial.println(" rad/s");

  Serial.print("Temperature: ");
  Serial.print(temp.temperature);
  Serial.println(" degC");

  //Formula to read angles

  double xAngle = atan( (a.acceleration.x) / (sqrt(sq(a.acceleration.y) + sq(a.acceleration.z))));
  double yAngle = atan( a.acceleration.y / (sqrt(sq(a.acceleration.x) + sq(a.acceleration.z))));
  double zAngle = atan( sqrt(sq(a.acceleration.x) + sq(a.acceleration.y)) / a.acceleration.z);

  xAngle *= 180.00;
  yAngle *= 180.00;
  zAngle *= 180.00;

  Serial.println("");

  Serial.print(" angle X: ");
  Serial.print( xAngle);

  Serial.print(", angle Y: ");
  Serial.print( yAngle);

  Serial.print(", angle Z: ");
  Serial.print( zAngle);

  Serial.println("");



  if ((xAngle >= 3 || xAngle <= (-3)))
  {
    digitalWrite(RELAY, HIGH);
    Serial.print("3 degree change in x");
  }
  else
  {
    digitalWrite(RELAY, LOW);
  }


  if ((yAngle >= 3 || yAngle <= (-3)))
  {
    digitalWrite(RELAY, HIGH);
    Serial.print("3 degree change in y");

  }
  else
  {
    digitalWrite(RELAY, LOW);
  }


  if (digitalRead(SW1) == LOW)
  {
    delay(20);

    if (digitalRead(SW1) == LOW)
    {
      state = 0;
      Serial.println("Recalibarations");
    }
  }



delay(20);

 

}

///////////////////////////////////   FUNCTIONS   ////////////////////////////////////
void meansensors() {
  long i = 0, buff_ax = 0, buff_ay = 0, buff_az = 0, buff_gx = 0, buff_gy = 0, buff_gz = 0;

  while (i < (buffersize + 101)) {
    // read raw accel/gyro measurements from device
    accelgyro.getMotion6(&ax, &ay, &az, &gx, &gy, &gz);

    if (i > 100 && i <= (buffersize + 100))
    { //First 100 measures are discarded
      buff_ax = buff_ax + ax;
      buff_ay = buff_ay + ay;
      buff_az = buff_az + az;
      buff_gx = buff_gx + gx;
      buff_gy = buff_gy + gy;
      buff_gz = buff_gz + gz;
    }
    if (i == (buffersize + 100))
    {
      mean_ax = buff_ax / buffersize;
      mean_ay = buff_ay / buffersize;
      mean_az = buff_az / buffersize;
      mean_gx = buff_gx / buffersize;
      mean_gy = buff_gy / buffersize;
      mean_gz = buff_gz / buffersize;
    }
    i++;
    delay(2); //Needed so we don't get repeated measures
  }
}

void calibration()
{
  ax_offset = -mean_ax / 8;
  ay_offset = -mean_ay / 8;
  az_offset = (16384 - mean_az) / 8;

  gx_offset = -mean_gx / 4;
  gy_offset = -mean_gy / 4;
  gz_offset = -mean_gz / 4;
  readOffsetFromEEPROM();
  while (1) {
    int ready = 0;
    accelgyro.setXAccelOffset(ax_offset);
    accelgyro.setYAccelOffset(ay_offset);
    accelgyro.setZAccelOffset(az_offset);

    accelgyro.setXGyroOffset(gx_offset);
    accelgyro.setYGyroOffset(gy_offset);
    accelgyro.setZGyroOffset(gz_offset);

    meansensors();
    Serial.println("...");

    if (abs(mean_ax) <= acel_deadzone) ready++;
    else ax_offset = ax_offset - mean_ax / acel_deadzone;

    if (abs(mean_ay) <= acel_deadzone) ready++;
    else ay_offset = ay_offset - mean_ay / acel_deadzone;

    if (abs(16384 - mean_az) <= acel_deadzone) ready++;
    else az_offset = az_offset + (16384 - mean_az) / acel_deadzone;

    if (abs(mean_gx) <= giro_deadzone) ready++;
    else gx_offset = gx_offset - mean_gx / (giro_deadzone + 1);

    if (abs(mean_gy) <= giro_deadzone) ready++;
    else gy_offset = gy_offset - mean_gy / (giro_deadzone + 1);

    if (abs(mean_gz) <= giro_deadzone) ready++;
    else gz_offset = gz_offset - mean_gz / (giro_deadzone + 1);

    if (ready == 6) break;
  }
}

void writeOffsetToEEPROM()
{
  int address = 0;
  EEPROM.write(address, ax_offset );//EEPROM.put(address, param);
  EEPROM.write(address + 1, ax_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(address + 2, ay_offset ); //EEPROM.put(address, param);
  EEPROM.write(address + 3, ay_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(address + 4, az_offset ); //EEPROM.put(address, param);
  EEPROM.write(address + 5, az_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(address + 6, gx_offset ); //EEPROM.put(address, param);
  EEPROM.write(address + 7, gx_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(address + 8, gy_offset ); //EEPROM.put(address, param);
  EEPROM.write(address + 9, gy_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(address + 10, gz_offset ); //EEPROM.put(address, param);
  EEPROM.write(address + 11, gz_offset >> 8); //EEPROM.put(address, param);
  EEPROM.write(12, EEPROM_SENSOR_STATE);
  EEPROM.commit();


}

void readOffsetFromEEPROM()
{
  Serial.print("Printing Perivious Offsets \t");
  int address = 0;
  int16_t EEPROMRead[6];
  for (int aa = 0; aa < 7; aa++)
  {
    EEPROMRead[aa] = (EEPROM.read(address + 1) << 8);
    EEPROMRead[aa] |= EEPROM.read(address);
    address = address + 2;
    if (EEPROMRead[aa] < 0)
    {
      EEPROMRead[aa] = EEPROMRead[aa] - 65536;
      Serial.print(EEPROMRead[aa]);
      Serial.print("\t");
    }
    else
    {
      Serial.print(EEPROMRead[aa]);
      Serial.print("\t");
    }
  }
  //  EEPROMRead[0] = ax_offset;
  //  EEPROMRead[1] = ay_offset;
  //  EEPROMRead[2] = az_offset;
  //  EEPROMRead[3] = gx_offset;
  //  EEPROMRead[4] = gy_offset;
  //  EEPROMRead[5] = gz_offset;

  ax_offset = EEPROMRead[0];
  ay_offset = EEPROMRead[1];
  az_offset = EEPROMRead[2];
  gx_offset = EEPROMRead[3];
  gy_offset = EEPROMRead[4];
  gz_offset = EEPROMRead[5];

  int checkstate = EEPROM.read(12);
  Serial.print("Current state is");
  Serial.print("\t");
  Serial.print(checkstate);

  //  int readoffset_x;
  //  readoffset_x = (EEPROM.read(1) << 8);
  //  readoffset_x |= EEPROM.read(0);
  //
  //
  //  Serial.print("Read offset_x = ");
  //  if (ax_offset < 0)
  //  {
  //    readoffset_x = readoffset_x - 65536;
  //    Serial.println(readoffset_x);
  //  }
  //  else
  //  {
  //    Serial.println(readoffset_x);
  //
  //
  //  }
}
